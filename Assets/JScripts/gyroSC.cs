﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gyroSC : MonoBehaviour
{
    private const float lowPassFilterFactor = 0.2f;
    Gyroscope go;
    bool gyinfo;

    public Text test;
    public Text test_1;
    public GameObject boy;
    public GameObject girl;
    public bool rotat;
    Vector3 deviceGravity;

    // Start is called before the first frame update
    void Start()
    {
        gyinfo = SystemInfo.supportsGyroscope;
        go = Input.gyro;
        //设置设备陀螺仪的开启/关闭状态，使用陀螺仪功能必须设置为 true          
        Input.gyro.enabled = true;
        
        //设备的旋转速度，返回结果为x，y，z轴的旋转速度，单位为（弧度/秒）          
        Vector3 rotationVelocity = Input.gyro.rotationRate;
        //获取更加精确的旋转          
        Vector3 rotationVelocity2 = Input.gyro.rotationRateUnbiased;
        //设置陀螺仪的更新检索时间，即隔 0.1秒更新一次          
        Input.gyro.updateInterval = 0.1f;
        //获取移除重力加速度后设备的加速度          
        Vector3 acceleration = Input.gyro.userAcceleration;
    }

    // Update is called once per frame
    void Update()
    {
        //获取设备重力加速度向量          
        deviceGravity = Input.gyro.gravity;
        //Input.gyro.attitude 返回值为 Quaternion类型，即设备旋转欧拉角          
        transform.rotation = Quaternion.Slerp(transform.rotation, Input.gyro.attitude, lowPassFilterFactor);
        //transform.rotation = Input.gyro.attitude * new Quaternion(0, 0, 1, 0);
        //transform.localEulerAngles = new Vector3(0,0,0);
        //transform.rotation=Quaternion.Euler(Input.gyro.rotationRateUnbiased);

        Debug.Log(deviceGravity.x);
        Debug.Log(deviceGravity.y);
        test.text = deviceGravity.x.ToString();
        //test_1.text = deviceGravity.y.ToString();


        if (deviceGravity.x <= 0.2f && rotat == true)
        {
            boy.transform.eulerAngles = new Vector3(0,0,0);
            girl.transform.eulerAngles = new Vector3(0, 0, 0);
            rotat = false;
            test_1.text = "false";
        }
        if (deviceGravity.x >= 0.8f && rotat == false)
        {
            boy.transform.eulerAngles = new Vector3(0, 0, 90);
            girl.transform.eulerAngles = new Vector3(0, 0, 90);
            rotat = true;
            test_1.text = "true";
        }
    }
}
