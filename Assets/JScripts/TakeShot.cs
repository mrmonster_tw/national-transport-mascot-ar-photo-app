﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

public class TakeShot : MonoBehaviour
{
    public GameObject loading;
    public GameObject shot;
    public GameObject hint;
    public GameObject quit;
    public GameObject target;
    public GameObject pic;
    public GameObject scan_cube;
    public GameObject setting;

    public GameObject ani;
    public GameObject boy;
    public GameObject girl;
    public GameObject boy_ani;
    public GameObject girl_ani;
    public int once;
    public float timef;
    public bool time_on;

    public Camera cam;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(camwait_fn());

        #if PLATFORM_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
        {
            Permission.RequestUserPermission(Permission.ExternalStorageWrite);
        }
        #endif
    }

    // Update is called once per frame
    void Update()
    {
        /*if (Screen.orientation == ScreenOrientation.LandscapeLeft)
        {
            girl.transform.localEulerAngles = new Vector3(0, 0, 90);
        }
        if (Screen.orientation == ScreenOrientation.Portrait)
        {
            girl.transform.localEulerAngles = new Vector3(0,0,0);
        }*/

        if (time_on && timef < 10f)
        {
            timef = timef + 1f * Time.deltaTime;
        }
        if (target.activeSelf && once == 0)
        {
            time_on = true;
            pic.SetActive(true);
            scan_cube.GetComponent<Animator>().Play("out");
            once = 1;
        }

        if (Input.GetMouseButton(0) && ani.activeSelf)
        {
            time_on = false;
            setting.SetActive(true);
            boy_ani.GetComponent<Animator>().speed = 0;
            girl_ani.GetComponent<Animator>().speed = 0;
            this.GetComponent<AudioSource>().Pause();
        }

        if (timef >= 10f)
        {
            time_on = false;
            timef = 0;
            quit.SetActive(true);
            shot.SetActive(true);

            ani.SetActive(false);
            boy.SetActive(true);
            girl.SetActive(true);
            scan_cube.SetActive(false);
        }
    }
    public void time_go()
    {
        time_on = true;
    }
    public void takeshot()
    {
        StartCoroutine(scanAndRefresh());
    }
    private IEnumerator camwait_fn()
    {
        yield return new WaitForSeconds(0.5f);
        cam.orthographic = true;
        yield return new WaitForSeconds(1f);
        once = 0;
        loading.SetActive(false);
    }
    private IEnumerator scan()
    {
        shot.SetActive(false);
        quit.SetActive(false);
        Texture2D t2D = new Texture2D(Screen.width, Screen.height);//掃描後的影像儲存大小，越大會造成效能消耗越大，若影像嚴重延遲，請降低儲存大小。
        //Texture2D t2D = new Texture2D(300, 300);//掃描後的影像儲存大小，越大會造成效能消耗越大，若影像嚴重延遲，請降低儲存大小。
        yield return new WaitForEndOfFrame();//等待攝影機的影像繪製完畢

        t2D.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);//掃描的範圍，設定為整個攝影機拍到的影像，若影像嚴重延遲，請降低掃描大小。
        //t2D.ReadPixels(new Rect(480, 240, 400, 400), 0, 0, false);
        t2D.Apply();//開始掃描

        byte[] byt = t2D.EncodeToPNG();
        //string path = Application.persistentDataPath.Substring(0, Application.persistentDataPath.IndexOf("Android")) + "/sdcard/DCIM/";
        string path = Application.persistentDataPath.Substring(0, Application.persistentDataPath.IndexOf("Android")) + "DCIM/Camera/";
        if (!Directory.Exists(path))
        {
        Directory.CreateDirectory(path);
        }
        File.WriteAllBytes(path + System.DateTime.Now.ToString("MMddHHmmss") + ".png", byt);
        string[] paths = new string[1];
        paths[0] = path;

        yield return new WaitForSeconds(1);
        ScanFile(paths);
        hint.SetActive(true);
        yield return new WaitForSeconds(3);
        ScanFile(paths);
        hint.SetActive(false);
        shot.SetActive(true);
        quit.SetActive(true);
    }

    private IEnumerator scanAndRefresh()
    {
        Debug.Log("scanAndRefresh");

        shot.SetActive(false);
        quit.SetActive(false);

        yield return new WaitForEndOfFrame();

        string path = Application.persistentDataPath.Substring(0, Application.persistentDataPath.IndexOf("Android")) + "DCIM/Camera/";
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        path += System.DateTime.Now.ToString("MMddHHmmss") + ".png";
        //Debug.Log("path=" + path);

        Texture2D screenImage = new Texture2D(Screen.width, Screen.height);
        //Get Image from screen
        screenImage.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        screenImage.Apply();
        //Convert to png
        byte[] imageBytes = screenImage.EncodeToPNG();

        //Save image to file
        System.IO.File.WriteAllBytes(path, imageBytes);
        yield return new WaitForSeconds(1);

        string[] paths = new string[1];
        paths[0] = path;
        ScanFile(paths);
        hint.SetActive(true);

        yield return new WaitForSeconds(3);
        ScanFile(paths);
        hint.SetActive(false);
        shot.SetActive(true);
        quit.SetActive(true);
    }

    void ScanFile(string[] path)
    {
        using (AndroidJavaClass PlayerActivity = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            AndroidJavaObject playerActivity = PlayerActivity.GetStatic<AndroidJavaObject>("currentActivity");
            using (AndroidJavaObject Conn = new AndroidJavaObject("android.media.MediaScannerConnection", playerActivity, null))
            {
                Conn.CallStatic("scanFile", playerActivity, path, null, null);
            }
        }
    }
    public void closegame_fn()
    {
        Application.Quit();
    }
}
